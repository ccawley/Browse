/* Copyright 1998 Acorn Computers Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**************************************************************/
/* File:    Markers.h                                         */
/*          (C) 2000 Pace Micro Technology PLC                */
/*          All rights reserved                               */
/*                                                            */
/* Purpose: Remember what's at the top of the visible area in */
/*          a browser window and possibly jump back there     */
/*          later on.                                         */
/*                                                            */
/* Author:  A.D.Hodgkinson.                                   */
/*                                                            */
/* History: 24-Mar-1998 (ADH): Created.                       */
/*          07-Apr-2000 (ADH): 64-wide comments adopted.      */
/**************************************************************/

#ifndef Browser_Markers__
  #define Browser_Markers__

  #include <kernel.h>

  #include "Global.h"

  /* Function prototypes */

  void              markers_init                  (void);
  void              markers_init_front_end        (browser_data * b);

  _kernel_oserror * markers_set_marker            (browser_data * b, int marker);
  _kernel_oserror * markers_remember_position     (browser_data * b);
  void              markers_forget_position       (browser_data * b);
  int               markers_set                   (browser_data * b, int marker);

  _kernel_oserror * markers_jump_to_marker        (browser_data * b, int marker);

  _kernel_oserror * markers_clear_marker_if_owned (browser_data * b, int marker);
  _kernel_oserror * markers_clear_marker          (int marker);
  _kernel_oserror * markers_clear_markers         (browser_data * b);
  _kernel_oserror * markers_clear_all             (void);

#endif /* Browser_Markers__ */
